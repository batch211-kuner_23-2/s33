//console.log('Hello World');

// JAVASCRIPT SYNCHRONOUS AND ASYNCHRONOUS
/*
SYNCHRONOUS
-synchronous codes runs in sequence. This means that an operation must wait for the previous one to complete before executing.

ASYNCHRONOUS
- code runs in parallel. This means that an operation can occur while another one is still being processed. 
*/

// Example:

console.log("Hello World")
//cnsole.log("Hello Again")


// CONCEPT OF SYNCHRONOUS
// for (let i = 0; i <= 5; i++) {
// 	console.log(i)
// }

// console.log("Hello It's Me")

/*
API stands for APPLICATION PROGRAMMING INTERFACE
-an application programming interface is a particular set of codes that allows software programd to communicate with each other.

-an API is the interface through which you access someone else's code or through which someone else code accesses yours.

Example:

Google API's
https://developers.google.com/identity/sign-in/web/sign-in

Youtube API's
https://developers.google.com/youtube/iframe_api_reference
*/

// FETCH API

	// console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
/*
-	A "promise" is an object represents the eventual completionor failure of an asynchronous function and it's resulting value.

A promise is in one of these three states:
Pending:
- initial state, neither fulfilled nor rejected.
Fulfilled:
- operation was successfully completed.
Rejected:
- operation failed.
*/


/*
-	by using the ".then" method, we can now check for the status of the promise.
-	the "fetch" method will return a "promise" that resolves to be a "response" object
- the ".then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected".
*/

// fetch('https://jsonplaceholder.typicode.com/posts')
// .then(response => console.log(response.status))

// fetch('https://jsonplaceholder.typicode.com/posts')
// // use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application.
// .then((response) => response.json())
// // print the converted JSON value from the "fetch" request
// // using multiple ".then" method to create a promise chain
// .then((json) => console.log(json))


/*
-	The "async" and "await" keywords is another approach that can be used achieve asynchronous codes.
-	used in functions to indicate which portions of code should be waited
-
*/

// async function fetchData() {
// 	// waits for the fetch method to stores the value in the "result" variable
// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
// 	// result returned by fetch is returned as a promise
// 	console.log(result);
// 	// the returned "response" is an object
// 	console.log(typeof result);
// 	// We cannot access the content of the "reponse" by directly accessing it's body property
// 	console.log(result.body)
// 	let json = await result.json();
// 	console.log(json);
// };
// fetchData();



// GETTING A SPECIFIC POST
// Retrieves a specific post following the Rest API ( retrieved, /post/id:, GET)

// fetch('https://jsonplaceholder.typicode.com/posts/1')
// .then((response) => response.json())
// .then((json) => console.log(json));


/*
POSTMAN

url: https://jsonplaceholder.typicode.com/posts/1
method: GET
*/

/*
CREATING A POST
SYNTAX:
fetch('URL',options)
.then((response) => {})
.then((response) => {})
*/

// CREATE A NEW POST FOLLOWING THE REST API (create, /post/:id, POST)

// fetch('https://jsonplaceholder.typicode.com/posts',{
// 	method: 'POST',
// 	headers: {
// 		'Content-Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		title: 'New posts',
// 		body: 'Hello World',
// 		userId: 1
// 	})
// })
// .then((response) => response.json())
// .then((json) => console.log(json))


/*
POSTMAN
url: https://jsonplaceholder.typicode.com/posts
method: POST
body: raw + JSON
				{
				    "title": "My First Blog Post",
				    "body": "Hello world!",
				    "userId": 1
				}
*/


// UPDATING A POST
// updates a specific post following the Rest API (update, /post/:id, PUT)

// fetch('https://jsonplaceholder.typicode.com/posts/1',{
// 	method: 'PUT',
// 	headers: {
// 		'Content-Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		id: 1,
// 		title: 'Updated posts',
// 		body: 'Hello Again!',
// 		userId: 1
// 	})
// })
// .then((response) => response.json())
// .then((json) => console.log(json))

/*
POSTMAN

url: https://jsonplaceholder.typicode.com/posts/1
method: PUT
body: raw + JSON
				{
				    "title": "My First Revised Blog Post",
				    "body": "Hello there! I revised this a bit",
				    "userId": 1
				}
*/


// UPDATE A POST USING THE PATCH METHOD
/*
-	Updates a specific post following the Rest API(update, /post/:id,Patch)
-	 patch updates the parts
*/

// fetch('https://jsonplaceholder.typicode.com/posts/1',{
// 	method: 'PATCH'
// 	headers: {
// 		'Content-Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		id: 1,
// 		title: 'Corrected post',
// 	})
// })
// .then((response) => response.json())
// .then((json) => console.log(json));

/*
POSTMAN

url: https://jsonplaceholder.typicode.com/posts/1
method: PATCH
body: raw + JSON
				{
				    "title": "This is my final title"
				}
*/

// DELETING POST
// Deleting a specific post following the Rest API(update, /post/:id,DELETE)

// fetch('https://jsonplaceholder.typicode.com/posts/1' {
// 	method: 'DELETE'
// })

/*
POSTMAN

url: https://jsonplaceholder.typicode.com/posts/1
method: DELETE
*/


// FILTERING THE POST
/*
-	The data can be filtered by sending the userId along with the URL
-	information sent via URL can be done by adding the question mark symbol ( ? )

Snytax:
Individual Parameters:
'url?parameterName=value'
Multiple Parameters:
'url?paramA=valueA&paramB=valueB'
*/

// fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
// .then((response) => response.json())
// .then((json) => console.log(json));

// fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3')
// .then((response) => response.json())
// .then((json) => console.log(json));


// RETRIEVE COMMENTS OF A SPECIFIC POST
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));