//console.log('Hello World')

// fetch('http://jsonplaceholder.typicode.com/todos/1')
// .then((response) => response.json())
// .then((json) => console.log(json));

//GETTING ALL TO DO LIST ITEM
// fetch('http://jsonplaceholder.typicode.com/todos')
// .then((response) => response.json())
// .then((json) => {

// 	let list = json.map((todo => {
// 		return todo.title;
// 	}))

// 	console.log(list)
// }

// // GETTING A SPECIFIC TO DO LIST ITEM
// fetch('http://jsonplaceholder.typicode.com/todos/1')
// .then((response) => response.json())
// .then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// // CREATE A TO DO LIST ITEM USING POST METHOD
// fetch('http://jsonplaceholder.typicode.com/todos'{
// 	method: 'POST',
// 	headers: {
// 		'Content Type': 'application/json'
// 	},
// 	body.JSON.stringify({
// 		title: 'Created To Do List Item',
// 		completed: false,
// 		userId: 1
// 	})
// })
// .then((response) => response.json())
// .then((json) => console.log(json));


// // UPDATE A TO DO LIST ITEM USING PUT METHOD
// fetch('http://jsonplaceholder.typicode.com/todos/1'{
// 	method: 'POST',
// 	headers: {
// 		'Content Type': 'application/json'
// 	},
// 	body.JSON.stringify({
// 		title: 'Updated To Do List Item',
// 		description: 'To update the my todo list with a different data structure.',
// 		status: 'Pending',
// 		dateCompleted: 'Pending',
// 		userId: 1
// 	})
// })
// 	.then((response) => response.json())
// 	.then((json) => console.log(json));

// // UPDATING A TO DO LIST ITEM USING PATCH METHOD
// fetch('http://jsonplaceholder.typicode.com/todos/1'{
// 	method: 'PATCH',
// 	headers: {
// 		'Content Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		status: "Completed",
// 		dataCompleted: "07/09/21"
// 	})
// })
// 	.then((response) => response.json())
// 	.then((json) => console.log(json))

// // // DELETE A TO DO LIST
// fetch('http://jsonplaceholder.typicode.com/todos', {
// 	method: 'DELETE'
// })
